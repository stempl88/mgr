<?php

namespace Core\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as GEDMO;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Webpage
 *
 * @ORM\Table(name="webpage")
 * @ORM\Entity
 */
class Webpage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * 
     * @ORM\Column(type="string", length=128)
     */
    private $domain;
    
    /**
     * 
     * @ORM\Column(type="object", nullable=true)
     */
    private $allowed_subdomain;
    
    /**
     * 
     * @ORM\Column(type="integer", length=5)
     * @Assert\Max(limit=10)
     */
    private $deep;
    
    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Webpage
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set allowed_subdomain
     *
     * @param \stdClass $allowedSubdomain
     * @return Webpage
     */
    public function setAllowedSubdomain($allowedSubdomain)
    {
        $this->allowed_subdomain = $allowedSubdomain;
    
        return $this;
    }

    /**
     * Get allowed_subdomain
     *
     * @return \stdClass 
     */
    public function getAllowedSubdomain()
    {
        return $this->allowed_subdomain;
    }

    /**
     * Set deep
     *
     * @param integer $deep
     * @return Webpage
     */
    public function setDeep($deep)
    {
        $this->deep = $deep;
    
        return $this;
    }

    /**
     * Get deep
     *
     * @return integer 
     */
    public function getDeep()
    {
        return $this->deep;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Webpage
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Webpage
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}