<?php

namespace Core\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskEntry
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TaskEntry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
