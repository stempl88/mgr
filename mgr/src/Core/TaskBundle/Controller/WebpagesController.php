<?php

namespace Core\TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Core\TaskBundle\Entity\Webpage;
use Core\TaskBundle\Form\WebpageType;

class WebpagesController extends Controller
{
	public function indexAction()
	{
		$entity = new Webpage();
		$form = $this->createForm(new WebpageType, $entity);
		return $this->render('CoreTaskBundle:Webpages:index.html.twig',array('form_new_page'=>$form->createView()));
	}
}
