<?php

namespace Core\TaskBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WebpageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('domain','text',array('attr'=>array('placeholder'=>'http://www.example.com')))
            ->add('allowed_subdomain','field',array('label'=>'Dozwolone subdomeny:'))
            ->add('deep','integer',array('label'=>'Głębokość zagnieżdzenia:','label_attr'=>array('data-role'=>'helper','title'=>'Do jakiego poziomy zagnieżdzenia,<br/>strona będzie przeszukiwana.<br/><br/>Im większa liczba, tym dłużej będzie<br/>trwało skanowanie.')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Core\TaskBundle\Entity\Webpage'
        ));
    }

    public function getName()
    {
        return 'webpage';
    }
}
