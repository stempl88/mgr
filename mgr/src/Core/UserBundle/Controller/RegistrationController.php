<?php
namespace Core\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;

class RegistrationController extends BaseController {

	public function homePageBoxAction()
	{
		$form = $this->container->get('fos_user.registration.form');
		
		return $this->container->get('templating')->renderResponse('CoreUserBundle:Registration:homePageBox.html.twig',
				array('form'=>$form->createView())
				);
	}
}
