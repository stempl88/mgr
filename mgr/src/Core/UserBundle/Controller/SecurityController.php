<?php
namespace Core\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\Security\Core\SecurityContext;

class SecurityController extends BaseController
{
	
	public function loginBoxAction()
	{
		$request = $this->container->get('request');
		/* @var $request \Symfony\Component\HttpFoundation\Request */
		$session = $request->getSession();
		/* @var $session \Symfony\Component\HttpFoundation\Session */
		$lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);
		
		$csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');
		
		return $this->container->get('templating')->renderResponse('CoreUserBundle:Security:loginBox.html.twig', 
				array(
				'last_username' => $lastUsername,
				'csrf_token' => $csrfToken,)
				);
	}
}
