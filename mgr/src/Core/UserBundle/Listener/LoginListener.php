<?php
namespace Core\UserBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListener {

	private $router;
	private $security;
	private $dispatcher;
	
	public function __construct(Router $router, SecurityContext $security, EventDispatcher $dispatcher)
	{
		$this->router = $router;
		$this->security = $security;
		$this->dispatcher = $dispatcher;
	}
	
	public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
	{
		$this->dispatcher->addListener(KernelEvents::RESPONSE, array($this, 'onKernelResponse'));
	}
	
	public function onKernelResponse(FilterResponseEvent $event)
	{
		if ($this->security->isGranted('ROLE_USER')) {
			$response = new RedirectResponse($this->router->generate('fos_user_profile_show'));
		} else {
			$response = new RedirectResponse($this->router->generate('homepage'));
		}
	
		$event->setResponse($response);
	}
}
