<?php
namespace Core\MainBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware 
{
	
	public function mainMenu(FactoryInterface $factory, array $options)
	{
		$menu = $factory->createItem('root',array('childrenAttributes'=>array('class'=>'menu')));
		$menu->addChild('Home',array('route'=>'homepage'));
		$menu->addChild('Konto',array('route'=>'fos_user_profile_show'));
		if($this->container->get('security.context')->isGranted('ROLE_USER'))
		{
			$menu->addChild('Wyloguj',array('route'=>'fos_user_security_logout'));
		}
		return $menu;
	}
	
	public function profileMenu(FactoryInterface $factory, array $options)
	{
		$menu = $factory->createItem('root',array('childrenAttributes'=>array('class'=>'menu')));
		$menu->addChild('Dashboard',array('route'=>'fos_user_profile_show'));
		
		// konfiguracja menu Zadania
		$menu->addChild('task',array(
				'uri'=>'#',
				'label'=>'Zadania',
				'childrenAttributes'=>array('class'=>'dropdown-menu'), // klasa dla obiektu "ul" podmenu
				'attributes'=>array(
						'data-role'=>'dropdown'
						)
				));
			$menu['task']->addChild('Zarządzanie stronami',array('route'=>'webpage_index'));
			$menu['task']->addChild('Lista zadań',array('route'=>'homepage'));
		$menu->addChild('Raporty',array('uri'=>'#'));
		
		
		return $menu;
	}
	
	private function _getUser()
	{
		return $this->container->get('security.context')->getToken()->getUser();
	}
}
