function initHomePageLoginBox() {
	$('#login-box').hover(function(){
		var $icon = $(this).find('i').first();
		$icon.data('oldClass',$icon.attr('class'))
				.removeClass()
					.addClass('icon-unlocked');
	},function(){
		$icon = $(this).find('i').first();
		$icon.removeClass()
				.addClass($icon.data('oldClass'));
	});
}

function initHomePageRegistationBox(){
	$('#form-homepage-registration').find('.input-control input').tipTip({activation: 'focus',defaultPosition:'top'});
	$('#form-homepage-registration').validate({
		
		submitHandler: function(form) {
		     //$(form).ajaxSubmit();
			return false;
		},
		rules: {
			'fos_user_registration_form[username]': "required",
			'fos_user_registration_form[email]': {required: true, email: true},
			'fos_user_registration_form[plainPassword][first]': "required",
			'fos_user_registration_form[plainPassword][second]': "required"
		}
	});
}


(function($){
	$('[data-role="helper"]').tipTip({defaultPosition:'top',maxWidth:'300px'});	
})(jQuery);

$(function(){
	function showOverlay(){
		$('#overlay').show();
	}
	function hideOverlay(){
		$('#overlay').hide();
	}
	window._originalAlert = window.alert;
	window.alert = function(text) {
		if ($('#alert').length){
			var modal = $('#alert')
			, content = modal.find('.content');
			
			content.html(text);
			modal.css('top',-1000);
			modal.show();
			var h = modal.outerHeight();
			showOverlay();
			modal.css('top',-h);
			modal.animate({top:0},200);
			modal.find('.close-modal').click(function(){
				modal.animate({'top':-h},200,'',hideOverlay);
			});
			
			return true;
		}  
	    window._originalAlert(text);
	};
	
	window._originalConfirm = window.confirm;
	
	window.confirm = function(text, cb) {
		if ($('#confirm').length){
			var modal = $('#confirm')
			, content = modal.find('.content');
			content.html(text);
			
			modal.css('top',-1000);
			modal.show();
			var h= modal.outerHeight();
			showOverlay();
			modal.css('top',-h);
			modal.animate({top:'30%'},400);
			modal.find('.actions button').click(function(){var btn=$(this);modal.animate({'top':-h},{duration:400,queue:false,complete:function(){hideOverlay();cb(btn.hasClass('confirm-modal'));}});btn.unbind('click');});
			return true;
		}
		window._originalConfirm(text);
	};
});